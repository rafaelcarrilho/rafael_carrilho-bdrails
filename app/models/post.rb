class Post < ApplicationRecord
    has_many :Like
    has_many :Comments

    validates:name, presence:true
    validates:message, presence:true

end
