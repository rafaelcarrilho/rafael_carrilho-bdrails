class Comment < ApplicationRecord
  belongs_to :Post
  validates:name, presence:true
  validates:message, presence:true
end
